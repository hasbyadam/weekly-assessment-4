//Write a function that converts hours into seconds.

const hoursToSecond = (sec) => {
    return sec * 3600
}

console.log(hoursToSecond(2))   // 7200
console.log(hoursToSecond(10))  // 36000
console.log(hoursToSecond(24))  // 86400
