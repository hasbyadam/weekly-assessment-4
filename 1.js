//Write a function that takes the base and height of a triangle and return its area.

const area = (base, height) => {
    return base*height/2
}

console.log(area(3, 2))     // 3
console.log(area(7, 4))     // 14
console.log(area(10, 10))   // 50
